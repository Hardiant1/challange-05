const express = require("express")
const app = express()
const fs = require("fs")
const multer = require("multer")
const path = require('path')
const {
    next
} = require("process")
const controller = require("./controllers/index");
const form = multer({dest: "public/uploads/"})

const {
    PORT = 8000
} = process.env

app.use(express.urlencoded({extended : false}))
app.set("view engine", "ejs");
app.use(express.static('./public'));

app.get("/" , controller.getList )
app.get("/form", controller.getForm)
app.post("/form", form.single("foto"), controller.postForm)
app.get("/update/:id", controller.getUpdate)
app.post("/update/:id", form.single("foto"), controller.postUpdate)
app.get("/delete/:id", controller.getDelete)


app.listen(PORT, () => {
    console.log(`express running on port ${PORT}`)
})