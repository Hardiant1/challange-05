'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class cars extends Model {
    static associate(models) {
      // define association here
      cars.hasOne(models.size, {
        foreignKey: "id_size"
      })
    }
  }
  cars.init({
    id_car: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    name: DataTypes.STRING,
    price: DataTypes.INTEGER,
    id_size: DataTypes.INTEGER,
    photo: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'cars',
  });
  return cars;
};