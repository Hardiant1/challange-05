# Car Management Dashboard
Kali ini saya membuat website Car Management Dashboard yang berfungsi untuk menampilkan, menambahkan, menghapus dan mengupdate list Car yang ada pada website Binar-Car-Rental

## How to run
Untuk menjalankan website ini cukup jalankan perintah "npm start" pada terminal/wsl dan kemudian buka mesin pencarian dan ketik "http://localhost:8000/


## Endpoints
1. app.get("/" , controller.getList ) --> berfungsi untuk menampilkan halaman dashboard (data mobil) .
2. app.get("/form", controller.getForm) --> berfungsi untuk menampilkan halaman form menginputan data mobil.
3. app.post("/form", form.single("foto"), controller.postForm) --> berfungsi untuk mengirim data ke database yang kemudian akan ditampilkan pada halaman dashboard.
4. app.get("/update/:id", controller.getUpdate) --> berfungsi untuk menampilkan halaman update mobil yang akan di update datanya.
5. app.post("/update/:id", form.single("foto"), controller.postUpdate) --> berfungsi untuk mengirimkan data baru kepada database agar data mobil yang di update dapat berubah.
6. app.get("/update/:id", controller.getUpdate) --> berfungsi untuk menampilkan halaman update form data mobil.
7. app.post("/update/:id", form.single("foto"), controller.postUpdate) --> berfungsi untuk mengirimkan data ke database dan kemudian akan diupdate menjadi data terbaru.
8. app.get("/delete/:id", controller.getDelete) --> berfungsi untuk menghapus mobil dari dashboard dan kemudian menampilkan list mobil yang tersisa.

## Directory Structure

```
.
├── config
│   └── config.json
├── controllers
│   └── index.js
├── migrations
│   ├── 20220424083437-create-size.js
│   └── 20220424084120-create-cars.js
├── models
│   ├── index.js
│   ├── cars.js
│   └── size.js
├── public
│   ├── css
│   ├── fonts
│   ├── img
│   ├── js
│   └── uploads
├── seeders
│   └── 20220424092927-size
├── views
│   ├── templates
│   │   ├── footer.ejs
│   │   └── header.ejs
│   ├── form.ejs
│   ├── list.ejs
│   └── update.ejs
├── .gitignore
├── README.md
├── index.js
├── package.json
```

## ERD
![Entity Relationship Diagram](public/img/erd.png)
