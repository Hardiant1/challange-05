// exports semua controller disini ...
const fs = require("fs");
const Cars = require("../models").cars;
const Size = require("../models").size;

module.exports = {
    getList: async (req, res) => {
        const size = req.query.size;
        if(size){
            const ukuran = await Size.findOne({
                where: {
                    name: size.toLowerCase()
                },
                attributes: [
                    "id_size"
                ]
            })
            const cars = await Cars.findAll({
                where: {
                    id_size: ukuran.id_size
                }
            })
            return res.render("list", {
                listCars: cars
            });
        }
        const cars = await Cars.findAll()
        res.render("list", {
            listCars: cars
        });
    },
    getForm: (req, res) => {
        res.render("form");
    },
    postForm: async (req, res) => {
        const size = await Size.findOne({
            where: {
                name: req.body.ukuran.toLowerCase()
            },
            attributes: [
                "id_size"
            ]
        })
        if (req.file) {
            let filename = req.file.originalname
            let temporary = req.file.path
            fs.copyFileSync(temporary, `public/uploads/${filename}`)
            fs.unlinkSync(temporary)

            await Cars.create({
                name: req.body.nama,
                price: req.body.sewa,
                id_size: size.id_size,
                photo: filename
            })
        }
        res.redirect("/");
    },
    getUpdate: async (req, res) => {
        const carId = req.params.id;
        const car = await Cars.findByPk(carId, {
            include: [{
                model: Size
            }]
        });
        const size = await Size.findByPk(car.id_size)
        car.size = size;
        console.log(car)
        res.render("update", {
            car: car
        });
    },
    postUpdate: async (req, res) => {
        const carId = req.params.id;
        const car = await Cars.findByPk(carId)
        let newPhoto = null;
        if (req.file) {
            let filename = req.file.originalname
            let temporary = req.file.path
            fs.copyFileSync(temporary, `public/uploads/${filename}`)
            fs.unlinkSync(temporary);
            if(fs.existsSync("public/uploads/"+ car.photo)){
                fs.unlinkSync("public/uploads/"+ car.photo);
            }
            newPhoto = filename;
        }
        const size = await Size.findOne({
            where:{
                name: req.body.ukuran.toLowerCase()
            }
        })
        console.log(size)
        await Cars.update({
            name: req.body.nama,
            price: req.body.sewa,
            id_size: size.id_size,
            photo: newPhoto ? newPhoto : car.photo
        }, {
            where: {
                id_car: carId
            }
        })
        res.redirect("/")
    },
    getDelete: async (req, res) => {
        const carId = req.params.id;
        await Cars.destroy({

            where: {
                id_car: carId
            }
        })
        res.redirect("/")
    }
}